<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 03.08.2016
 * Time: 13:26
 */

// src/AppBundle/Admin/BlogPostAdmin.php
namespace AppBundle\Admin;

use AppBundle\Entity\Content;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ContentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->tab('Содержание')
                ->with('Содержание')
                    ->add('name', 'text')
                    ->add('text', 'textarea' ,array('attr' => array('class' => 'tinymce')))
                    ->add('category', 'sonata_type_model', array(
                        'class' => 'AppBundle\Entity\Category',
                        'property' => 'name',
                    ))
                    ->add('date', 'datetime')
                ->end()
            ->end()



            ->tab('Публикация')
                ->with('Content')
                    ->add('status', 'choice',  array(
                        'label' => 'Статус',
                        'multiple' => false,
                        'choices' => array(
                            'Опубликовано' => 'published' ,
                            'Не опубликовано' =>  'notpublished',
                            'Архивное' =>  'archive',
                            'Без привязки' => 'nobind'
                        )))
                ->end()
            ->end()

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('category.name')
            ->add('date')
            ->add('status', 'choice', array(
                'choices' => array(
                    'published' =>   'Опубликовано',
                    'notpublished' => 'Не опубликовано',
                    'archive' =>  'Архивное',
                    'nobind' => 'Без привязки'
                )))

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
            ->add('category.name')
            ->add('date')
            ->add('status');
    }



    public function toString($object)
    {
        return $object instanceof Content
            ? $object->getName()
            : 'Статья'; // shown in the breadcrumb on the create view
    }
}