<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ServiceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')
            ->add('description', 'textarea' ,array('attr' => array('class' => 'tinymce')))
            ->add('price', 'integer')
            ->add('category', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Category',
                'property' => 'name',
            ))
            ->add('responsible', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\User',
                'property' => 'fio',
                "btn_add" => false,
            ))
            ->add('status', 'choice',  array(
                'label' => 'Статус',
                'multiple' => false,
                'choices' => array(
                    'Опубликовано' => 'published' ,
                    'Архивное' =>  'archive'

                )))
            ->add('availableAtSite', 'checkbox')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
}