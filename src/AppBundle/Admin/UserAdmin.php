<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')
            ->add('surname', 'text')
            ->add('patronymic', 'text')
            ->add('dateOfBirth', 'date')
            ->add('speciality', 'text')
            ->add('position', 'text')
            ->add('phone', 'text')
            ->add('mobilePhone', 'text')
            ->add('about', 'textarea' ,array('attr' => array('class' => 'tinymce')))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
}