<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 01.08.2016
 * Time: 17:47
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;

class AdminController extends Controller
{

    /**
     * @Route("admin/deleteFile{fileId}", name="deleteFile")
     */
    public function deleteFileAction($fileId)
    {
        $file = $this->getDoctrine()
            ->getRepository('AppBundle:File')
            ->find($fileId);
        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();
        return new JsonResponse('ok');
    }



}