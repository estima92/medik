<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $comment_repository = $this->getDoctrine()->getRepository('AppBundle:Comment');
        $query = $comment_repository->createQueryBuilder('c')
            ->where('c.isApproved = true')
            ->getQuery();
        $comments = $query->setMaxResults(2)->getResult();

        $dates = array();
        $today = date_create();
        for ($i = 1; $i <= 7; $i++) {
            $todayPlus = clone $today->add(new \DateInterval('P1D'));
            array_push($dates, $todayPlus);
        }

        $clinicInfo = $this->getDoctrine()->getRepository('AppBundle:siteInformation')->find(1);
        return $this->render('index.html.twig', array(
            'today' => $today,
            'comments' => $comments,
            'clinicInfo' => $clinicInfo,
        ));
    }

    /**
     * @Route("/complain", _name="complain")
     *
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ComplainType');

//        if ($request->isMethod('POST')) {
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Жалоба')
                ->setFrom($form->get('email')->getData())
                //n.talyzine@gmail.com
                ->setTo('hvatov@medico-s.ru')
                ->setBody(
                    $this->renderView(
                        ':elements:complainMessage.html.twig',
                        array(
                            'ip' => $request->getClientIp(),
                            'name' => $form->get('name')->getData(),
                            'phone' => $form->get('phone')->getData(),
                            'message' => $form->get('message')->getData()
                        )
                    )
                );

            $this->get('mailer')->send($message);
            return new Response('<p class="commentformresponse">Ваша жалоба принята,мы свяжемся с вами в ближайшее время!</p>');
        }
        return $this->render(':elements:complain.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/content/{link}", name="contentShow")
     */
    public function contentAction($link)
    {
        $content = $this->getDoctrine()->getRepository('AppBundle:Content')->createQueryBuilder('c')
            ->where('c.link = :link')
            ->setParameter('link', $link)
            ->getQuery()->getResult();
        $content = $content[0];
        return $this->render('content.html.twig', array(
            'content' => $content,
        ));
    }

    /**
     * @Route("/category", name="categories")
     */
    public function servicesAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        return $this->render('categories.html.twig', array('categories' => $categories));
    }

    /**
     * @Route("/category/{id}", name="categoryShow")
     */
    public function serviceAction(Request $request, $id)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);
        return $this->render('category.html.twig', array('category' => $category));
    }


    public function categoriesToNavbarAction()
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        return $this->render(':elements:categoriesToNavbar.html.twig', array('categories' => $categories));
    }


    /**
     * @Route("/chiefs", name="chiefs")
     */
    public function  chiefsAction()
    {
        $user_repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $query = $user_repository->createQueryBuilder('u')
            ->where('u.isBoss = true')
            ->getQuery();
        $specialists = $query
//            ->setMaxResults(6)
            ->getResult();

        return $this->render('chiefs.html.twig', array(
            'doctors' => $specialists,
        ));
    }

    /**
     * @Route("/specialists", name="specialists")
     */
    public function specialistsAction(Request $request)
    {
        $user_repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $query = $user_repository->createQueryBuilder('u')
            ->getQuery();
        $specialists = $query
//            ->setMaxResults(6)
            ->getResult();

        return $this->render('specialists.html.twig', array(
            'doctors' => $specialists,
        ));
    }



    /**
     * @Route("/specialist/{id}", name="specialist")
     */
    public function specialistAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        return $this->render('doctor.html.twig', array(
            'doctor' => $user,
        ));
    }

}
