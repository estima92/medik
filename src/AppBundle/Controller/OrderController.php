<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Order;
use DateInterval;
use DatePeriod;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;

/**
 * Order controller.
 *
 */
class OrderController extends Controller {

    /**
     * Get all doctors from medUnit.
     *
     * @Route("/getDoctorsWithMedUnit{medUnitId}", name="getDoctorsWithMedUnit")
     */
    public function getDoctorsWithMedUnitAction($medUnitId)
    {

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');

        //TODO move this ti repository
        $qb = $repository->createQueryBuilder('u')
            ->innerJoin('u.medUnit', 'm')
            ->where('m.id = :medUnitId')
            ->setParameter('medUnitId', $medUnitId)
            ->addOrderBy('u.fio', 'ASC');
        $query = $qb->getQuery();
        $doctors = $query->getResult();
        $doctorHTML = $this->renderView('order/doctors.html.twig', array(
            'doctors' => $doctors,
        ));
        $today = date_create();
        $todayPlusWeek = clone $today;
        $todayPlusWeek->add(new \DateInterval('P7D'));
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($today, $interval, $todayPlusWeek);
        $doctorsWeek = array ();
        foreach ( $doctors as $doctor ) {
            $doctorId = $doctor->getId();
            $parameters = array(
                'user_id' => $doctorId,
            );
            $doctorWeek = $this->arrayForSingleDoctorWithOrders($parameters);

            array_push($doctorsWeek,$doctorWeek);
        }
        $medUnitWeek = $doctorsWeek[0];
        foreach ($doctorsWeek as $week) {

            for ($i = 0; $i < 7; $i++) {
                for ($j = 0; $j < 10; $j++) {
                    $medUnitWeek[$i][$j] = $week[$i][$j] || $medUnitWeek[$i][$j];
                }
            }
        }

        $daysHTML = $this->renderView(':order:days.html.twig', array(
            'medUnitWeek' => $medUnitWeek,
            'today' => $today,
        ));
        $response = array(
        "doctorHTML" => $doctorHTML,
        "daysHTML" => $daysHTML
        );
        return new JsonResponse($response);
    }

    /**
     * Get all doctors available on hour
     *
     * @Route("/getAvailableDoctorsForOrder", name="getAvailableDoctorsForOrder")
     */
    public function getAvailableDoctorsForOrderAction(){

    }

    /**
     * Get Order Window
     *
     * @Route("/getOrderWindow", name="getOrderWindow")
     */
    public function getOrderWindowAction()
    {
        $today = date_create();
        $medUnits = $this->getDoctrine()->getRepository('AppBundle:medUnit')->findAll();
        return $this->render('order/order.html.twig', array(
            'medUnits' => $medUnits,
            'today' => $today,
        ));

    }


    /**
     * Get single Doctor
     *
     * @Route("/getDoctorTimetableWithOrders{doctorId}", name="getDoctorTimetableWithOrders")
     */
    public function getDoctorTimetableWithOrdersAction($doctorId){
        $today = date_create();
        $parameters = array(
            'user_id' => $doctorId,
        );
        $doctorWeek = $this->arrayForSingleDoctorWithOrders($parameters);

        $daysHTML = $this->renderView(':order:days.html.twig', array(
            'medUnitWeek' => $doctorWeek,
            'today' => $today,
        ));
        return new JsonResponse($daysHTML);
    }

    /**
     * Proceed Order.
     *
     * @Route("/proceedOrder", name="proceedOrder")
     */
    public function proceedOrderAction(Request $request){
        $day = $request->get('day');
        $hour = $request->get('hour');
        $userId = $request->get('doctor');
        $comment = $request->get('comment');
        $dataString = "".$day." ".$hour.":00";
        $startDay  = date_create_from_format("d.m.y G:i"  , $dataString);

        $order = new Order();
        $order->setStartDate($startDay);
        $medUnitId = $request->get('medUnit');
        $medUnit =  $this->getDoctrine()->getManager()->getRepository('AppBundle:medUnit')->find($medUnitId);
        $order->setMedUnit($medUnit);
//        if ( array_key_exists('userId', get_defined_vars()) ){
        if ( $userId != 0 ){
            $doctor =  $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->find($userId);
            $order->setDoctor($doctor);
        }

        $order->setFio($request->get('fio'));
        $order->setPhone($request->get('phone'));
        $order->setIsApproved(false);
        $order->setComment($comment);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new JsonResponse("ok!");
    }

    /**
     * Display all Orders
     *
     * @Route("/displayOrder", name="displayOrder")
     */
    public function displayOrderAction(){
       $orders = $this->getDoctrine()->getManager()->getRepository('AppBundle:Order')->findAll();
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
        $ordersWithReplace = array();
       foreach($orders as $order){
            $date = $order->getStartDate();
            $medUnit = $order->getMedUnit();
            $doctor = $order->getDoctor();
           $parameters = array(
               'medUnit' => $medUnit->getId(),
               'date' =>  date_format($date, 'Y-m-d H:i:s')
           );
           $qb = $repository->createQueryBuilder('u')
               ->innerJoin('u.medUnit', 'm')
               ->innerJoin('u.timetables', 't')
//               ->innerJoin('u.orders', 'o')
               ->where('m.id = :medUnit')
               ->andWhere(':date >= t.startDate ')
               ->andWhere(':date < t.endDate ')
//               ->andWhere(':date != o.startDate ')
               ->setParameters($parameters)
               ->addOrderBy('u.fio', 'ASC');
           $query = $qb->getQuery();
           $replaceDoctors = $query->getResult();

           $replaceDoctorsAvailable = array();
           foreach($replaceDoctors as $replaceDoctor){
               $replaceDoctorId = $replaceDoctor->getId();
               $replaceDoctorOrders = $this->getDoctrine()->getManager()->getRepository('AppBundle:Order')->createQueryBuilder('o')
                   ->innerJoin('o.doctor', 'u')
                   ->where('u.id = :doctorId')
                   ->setParameter('doctorId', $replaceDoctorId)
                   ->getQuery()->getResult();
               $a = 0;
               foreach ($replaceDoctorOrders as $replaceDoctorOrder){
                   $replaceDoctorOrderDate = $replaceDoctorOrder->getStartDate();
                   if ( $replaceDoctorOrderDate == $date) $a++;
               }

               if ($a == 0){
                   array_push($replaceDoctorsAvailable, $replaceDoctor );
               }

           }
           array_push($ordersWithReplace,[$order,$replaceDoctorsAvailable]);


//           $order = (array)$order;
//           $foo['replaceDoctors'] = $replaceDoctorsAvailable;
//           $order = (object)$order;

       }


       return $this->render(':order:orderList.html.twig', array(
            'orders' => $ordersWithReplace,
        ));
    }

    /**
     * switch Order Doctor
     *
     * @Route("/switchOrderDoctor", name="switchOrderDoctor")
     */
    public function switchOrderDoctorAction(Request $request){
        $newDoctorId = $request->get('newDoctorId');
        $orderId = $request->get('orderId');
        $order =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Order')->find($orderId);
        $newDoctor = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->find($newDoctorId);
        $order->setDoctor($newDoctor);
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();
        return new JsonResponse('Доктор изменен!');
    }

    /**
     * switch Order Status
     *
     * @Route("/switchOrderStatus", name="switchOrderStatus")
     */
    public function switchOrderStatusAction(Request $request){
        $orderId = $request->get('orderId');
        $comment = $request->get('comment');
        $order =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Order')->find($orderId);
        $order->setIsApproved(true);
        $order->setComment($comment);
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();
        return new JsonResponse('Визит подтвержден');

    }

    /**
     * Displays  an timetable for users in registration
     *
     * @Route("/getTimetableRegistration", name="getTimetableRegistration")
     * @Method({"GET", "POST"})
     */
    public function getTimetableRegistrationAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $doctors =  $em->getRepository('AppBundle:User')->findAll();
//        $today = getdate();
        $month = $request->get("month");
//        $month = $today['mon'];
//        $dataString = "".$day." ".$hour.":00";
        $monthDate  = date_create_from_format("m"  , $month);
        $maxDays = date_format($monthDate, "t");
        $timetablesWithOrders = array();

        $today = date_create();
        $todayPlusWeek = clone $today;
        $todayPlusWeek->add(new \DateInterval('P7D'));
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($today, $interval, $todayPlusWeek);



        foreach($doctors as $doctor) {
            $timetableWithOrders = array();
            $id = $doctor->getId();
            $parameters = array(
                'user_id' => $id,
                'currentMonth' => $month,
            );
            $result = $em->getRepository('AppBundle:Timetable')->findAllInCurrentMonth($parameters);
            $monthOrders = $em->getRepository('AppBundle:Order')->findAllInCurrentMonth($parameters);
            for( $i = 1; $i < $maxDays + 1 ; $i++ ) {
                $ordersForDay = array(); //[color of button, [orders]]
                $hasTimetable = 0;
                $dayOrders = array();
                foreach ($result as $a ){
                    $startDate = $a->getStartDate();
                    $startDay = intval($startDate->format('j'));
                    if ($i == $startDay)  {
                        $hasTimetable++;
                        $dayOrders = array();
                        foreach ($monthOrders as $monthOrder){
                            $orderStartDate = $monthOrder->getStartDate();
                            $orderStartDay = intval($orderStartDate->format('j'));
                            if ($i == $orderStartDay){
                               array_push($dayOrders, $monthOrder);
                            }
                        }
                    }
                }
                if ($hasTimetable > 0) {
                    if (count($dayOrders) > 0) {
                            array_push($ordersForDay, "yellow");
                            array_push($ordersForDay, $dayOrders);
                        }
                    else {array_push($ordersForDay, "green"); }

                }
                else {array_push($ordersForDay, "red");}
                array_push($timetableWithOrders,$ordersForDay);
            }
            array_push($timetablesWithOrders, [$doctor,$timetableWithOrders]);
        }
        $timetableWithOrdersHTML = $this->renderView(':user:registratura.html.twig', array(
            'timetableWithOrders' => $timetablesWithOrders,
        ));
        return new JsonResponse($timetableWithOrdersHTML);
    }

    /**
     * Checks order has doctor or not!
     *
     * @Route("/checkDoctorHasOrder", name="checkDoctorHasOrder")
     * @Method({"GET", "POST"})
     */
    public function checkOrderHasDoctor(Request $request){
        $orderId =  $request->get('orderId');
        $order = $this->getDoctrine()->getRepository('AppBundle:Order')->find($orderId);
        $doctor = $order->getDoctor();
        if ( $doctor == null ) { $isApproved = false;}
        else $isApproved = true;
        return new JsonResponse($isApproved);
    }


    public function arrayForSingleDoctorWithOrders($parameters){
        $today = date_create();
        $todayPlusWeek = clone $today;
        $todayPlusWeek->add(new \DateInterval('P7D'));
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($today, $interval, $todayPlusWeek);
        $doctorWeek = array ();

        $doctorTimetable =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Timetable')->findNearestWeek($parameters);
        $doctorOrders =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Order')->findNearestWeek($parameters);

        foreach ( $period as $dt ) {
            $orderHours = array();
            $workhours = array(); //[[period],[period]]
            foreach ( $doctorTimetable as $timetable ) {
                $startDate = $timetable->getStartDate();
                $endDate = $timetable->getEndDate();
                if (intval($startDate->format('j')) ==  intval($dt->format('j'))){
                    $startHour = intval($startDate->format('G'));
                    $endHour = intval($endDate->format('G'));
                    for ($startHour; $startHour < $endHour; $startHour++ ){
                        array_push($workhours,$startHour);
                    }
                }
            }
            foreach ( $doctorOrders as $order ) {
                $startDate = $order->getStartDate();
                if (intval($startDate->format('j')) ==  intval($dt->format('j'))){
                    $startHour = intval($startDate->format('G'));
                    array_push($orderHours,$startHour);
                }
            }
            $binaryWorkhours = array();
            for ($i = 9;$i < 19; $i++){
                $value = 0;
                foreach ($workhours as $elem){
                    if ($i == $elem){
                        $value = 1;
                        break;
                    }
                    else $value = 0;
                }
                array_push($binaryWorkhours,$value);
            }
            $binaryWorkhoursWithOrders = array();
            for ($i = 9;$i < 19; $i++){
                $value = 0;
                foreach ($orderHours as $elem){
                    if ($i == $elem){
                        $value = 1;
                        break;
                    }
                    else $value = 0;
                }
                if ($value == 1) array_push($binaryWorkhoursWithOrders, 0);
                else array_push($binaryWorkhoursWithOrders, $binaryWorkhours[$i - 9]);
            }
            array_push($doctorWeek,$binaryWorkhoursWithOrders);
        }
        return $doctorWeek;
    }
}