<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Timetable;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;


/**
 * User controller.
 *
 * @Route("/admin/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    /**
     * Displays  an timetable for User entity.
     *
     * @Route("/{id}/timetable", name="user_timetable")
     * @Method({"GET", "POST"})
     */
    public function userTimetableAction(Request $request,User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $today = getdate();
        $month = $today['mon'];
        $maxDays=date('t');
        $id = $user->getId();
        $parameters = array(
            'user_id' => $id,
            'currentMonth' => $month,
        );

        $result = $em->getRepository('AppBundle:Timetable')->findAllInCurrentMonth($parameters);

        $timetable = array();  // [day,workhours]
        for( $i = 1; $i < $maxDays + 1 ; $i++ ) {
            $workhours = array(); //[[period],[period]]
            foreach ($result as $a ){
              $startDate = $a->getStartDate();
              $endDate = $a->getEndDate();
              $startDay = intval($startDate->format('j'));
              if ($i == $startDay)  {
                  $startHour = intval($startDate->format('G'));
                  $endHour = intval($endDate->format('G'));
                  for ($startHour; $startHour < $endHour; $startHour++ ){
                      array_push($workhours,$startHour);
                  }
              }
            }
            array_push($timetable,$workhours);
        }


        return $this->render(':user:timetable.html.twig', array(
            'timetable' => $timetable,
            'user' => $user,
            'maxdays' => $maxDays,
            'month' => $month,
        ));
    }
    /**
     * Route for AJAX updating timetable
     *
     * @Route("/timetableUpdate", name="timetable_update")
     * @Method({"GET", "POST"})
     */
    public function timetableUpdateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $id = $request->get('id');
        $hours = $request->get('hours');
        $day = $request->get('day');
        $month = $request->get('month');
        $year = $request->get('year');
        $deleteFlag = $request->get('deleteFlag');
        $parameters = array(
            'user_id' => $id,
            'currentDay' => $day,
        );
        $result = $em->getRepository('AppBundle:Timetable')->findAllInCurrentDate($parameters);
        foreach ($result as $a ){
            $em->remove($a);
            $em->flush();
        }

        if ($deleteFlag == "true") {return new Response('ok');}
        $user = $em->getRepository('AppBundle:User')->find($id);
        $workHours = array();

        foreach($hours as $interval){
            $period = array();
            array_push($period,date_create($year.'/'.$month.'/'.$day.' '.$interval[0].':00'),date_create($year.'/'.$month.'/'.$day.' '.$interval[1].':00'));
            array_push($workHours,$period);
            $timetable = new Timetable();
            $timetable->setUser($user);
            $timetable->setStartDate($period[0]);
            $timetable->setEndDate($period[1]);

            $em->persist($timetable);
            $em->flush();
        };


//        foreach($workHours as )
//
//        $parameters = array(
//            'user_id' => $id,
//            'date_start' => $workHours[0][0]->format('Y-m-d 00:00:00'),
//            'date_end' => $workHours[0][0]->format('Y-m-d 23:59:59'),
//        );
//        $result = $this->getDoctrine()->getRepository('AppBundle:Timetable')->findAllinCurrentDate($parameters);
//        $query = $repo->createQueryBuilder('t')
//            ->innerjoin('t.user', 'u')
//            ->where('u.id = :user_id')
//            ->andWhere('t.startDate > :date_start')
//            ->andWhere('t.startDate < :date_end')
////            ->setParameter('date_start', $date->format('Y-m-d 00:00:00'))
////            ->setParameter('date_end',   $date->format('Y-m-d 23:59:59'));
//        ->setParameters($parameters)
//        ->getQuery();
//        $result = $query->getResult();





//        $statusStr = $request->get('status');
//        $status = filter_var($statusStr, FILTER_VALIDATE_BOOLEAN);
//        $em = $this->getDoctrine()->getManager();
//        $comment = $em->getRepository('AppBundle:Comment')->find($id);
//
//        $comment->setIsapproved($status);
//
//        $em->persist($comment);
//        $em->flush();




        return new Response('ok');
    }

    /**
     * Displays  an timetable for User entity.
     *
     * @Route("/getTimetable", name="getTimetable")
     * @Method({"GET", "POST"})
     */
    public function userGetTimetableAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $today = getdate();
        $month = $today['mon'];
        $maxDays=date('t');
        $parameters = array(
            'user_id' => $id,
            'currentMonth' => $month,
        );
        $result = $em->getRepository('AppBundle:Timetable')->findAllInCurrentMonth($parameters);
        return new Response('ok');

//        $this->render(':user:timetable.html.twig', array(
//            'user' => $user,
//            'maxdays' => $maxDays,
//            'month' => $month,
//        ));
    }

}
