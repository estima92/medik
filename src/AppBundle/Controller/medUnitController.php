<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\medUnit;
use AppBundle\Form\medUnitType;

/**
 * medUnit controller.
 *
 * @Route("/admin/medunit")
 */
class medUnitController extends Controller
{
    /**
     * Lists all medUnit entities.
     *
     * @Route("/", name="admin_medunit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $medUnits = $em->getRepository('AppBundle:medUnit')->findAll();

        return $this->render('admin/medunit/index.html.twig', array(
            'medUnits' => $medUnits,
        ));
    }

    /**
     * Creates a new medUnit entity.
     *
     * @Route("/new", name="admin_medunit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $medUnit = new medUnit();
        $form = $this->createForm('AppBundle\Form\medUnitType', $medUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medUnit);
            $em->flush();

            return $this->redirectToRoute('admin_medunit_show', array('id' => $medUnit->getId()));
        }

        return $this->render('admin/medunit/new.html.twig', array(
            'medUnit' => $medUnit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a medUnit entity.
     *
     * @Route("/{id}", name="admin_medunit_show")
     * @Method("GET")
     */
    public function showAction(medUnit $medUnit)
    {
        $deleteForm = $this->createDeleteForm($medUnit);

        return $this->render('admin/medunit/show.html.twig', array(
            'medUnit' => $medUnit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing medUnit entity.
     *
     * @Route("/{id}/edit", name="admin_medunit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, medUnit $medUnit)
    {
        $deleteForm = $this->createDeleteForm($medUnit);
        $editForm = $this->createForm('AppBundle\Form\medUnitType', $medUnit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medUnit);
            $em->flush();

            return $this->redirectToRoute('admin_medunit_edit', array('id' => $medUnit->getId()));
        }

        return $this->render('admin/medunit/edit.html.twig', array(
            'medUnit' => $medUnit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a medUnit entity.
     *
     * @Route("/{id}", name="admin_medunit_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, medUnit $medUnit)
    {
        $form = $this->createDeleteForm($medUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($medUnit);
            $em->flush();
        }

        return $this->redirectToRoute('admin_medunit_index');
    }

    /**
     * Creates a form to delete a medUnit entity.
     *
     * @param medUnit $medUnit The medUnit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(medUnit $medUnit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_medunit_delete', array('id' => $medUnit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
