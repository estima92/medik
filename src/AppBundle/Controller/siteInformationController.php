<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\siteInformation;
use AppBundle\Form\siteInformationType;

/**
 * siteInformation controller.
 *
 * @Route("/admin/siteinformation")
 */
class siteInformationController extends Controller
{
    /**
     * Lists all siteInformation entities.
     *
     * @Route("/", name="admin_siteinformation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $siteInformations = $em->getRepository('AppBundle:siteInformation')->findAll();

        return $this->render('admin/siteinformation/index.html.twig', array(
            'siteInformations' => $siteInformations,
        ));
    }

    /**
     * Creates a new siteInformation entity.
     *
     * @Route("/new", name="admin_siteinformation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $siteInformation = new siteInformation();
        $form = $this->createForm('AppBundle\Form\siteInformationType', $siteInformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($siteInformation);
            $em->flush();

            return $this->redirectToRoute('admin_siteinformation_show', array('id' => $siteInformation->getId()));
        }

        return $this->render('admin/siteinformation/new.html.twig', array(
            'siteInformation' => $siteInformation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a siteInformation entity.
     *
     * @Route("/{id}", name="admin_siteinformation_show")
     * @Method("GET")
     */
    public function showAction(siteInformation $siteInformation)
    {
        $deleteForm = $this->createDeleteForm($siteInformation);

        return $this->render('admin/siteinformation/show.html.twig', array(
            'siteInformation' => $siteInformation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing siteInformation entity.
     *
     * @Route("/{id}/edit", name="admin_siteinformation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, siteInformation $siteInformation)
    {
        $deleteForm = $this->createDeleteForm($siteInformation);
        $editForm = $this->createForm('AppBundle\Form\siteInformationType', $siteInformation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($siteInformation);
            $em->flush();

            return $this->redirectToRoute('admin_siteinformation_edit', array('id' => $siteInformation->getId()));
        }

        return $this->render('admin/siteinformation/edit.html.twig', array(
            'siteInformation' => $siteInformation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a siteInformation entity.
     *
     * @Route("/{id}", name="admin_siteinformation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, siteInformation $siteInformation)
    {
        $form = $this->createDeleteForm($siteInformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($siteInformation);
            $em->flush();
        }

        return $this->redirectToRoute('admin_siteinformation_index');
    }

    /**
     * Creates a form to delete a siteInformation entity.
     *
     * @param siteInformation $siteInformation The siteInformation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(siteInformation $siteInformation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_siteinformation_delete', array('id' => $siteInformation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
