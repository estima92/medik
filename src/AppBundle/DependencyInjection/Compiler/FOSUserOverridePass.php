<?php

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 09.08.2016
 * Time: 15:49
 */

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FOSUserOverridePass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $container->getDefinition('fos_user.listener.authentication')->setClass('AppBundle\EventListener\AuthenticationListener');
    }

}