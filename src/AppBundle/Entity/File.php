<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 02.08.2016
 * Time: 11:58
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class File
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\siteInformation", inversedBy="photo")
     */
    private $clinicInfoPhoto; //thats so bad so bad so bad so shitcode, i dont like it =(

    /**
     * @ORM\ManyToOne(targetEntity="siteInformation", inversedBy="certificates")
     */
    private $clinicInfoCertificate;

    /**
     * @ORM\ManyToOne(targetEntity="siteInformation", inversedBy="grants")
     */
    private $clinicInfoGrant;



    public function getAbsolutePath(){
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath(){
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir(){
        // the absolute directory path where uploadeddocuments should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir(){
        // get rid of the __DIR__ so it doesn't screw up when displaying uploaded doc/image in the view.
        return 'uploads';
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload(){
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set clinicInfoPhoto
     *
     * @param \AppBundle\Entity\siteInformation $clinicInfoPhoto
     *
     * @return File
     */
    public function setClinicInfoPhoto(\AppBundle\Entity\siteInformation $clinicInfoPhoto = null)
    {
        $this->clinicInfoPhoto = $clinicInfoPhoto;

        return $this;
    }

    /**
     * Get clinicInfoPhoto
     *
     * @return \AppBundle\Entity\siteInformation
     */
    public function getClinicInfoPhoto()
    {
        return $this->clinicInfoPhoto;
    }

    /**
     * Set clinicInfoCertificate
     *
     * @param \AppBundle\Entity\siteInformation $clinicInfoCertificate
     *
     * @return File
     */
    public function setClinicInfoCertificate(\AppBundle\Entity\siteInformation $clinicInfoCertificate = null)
    {
        $this->clinicInfoCertificate = $clinicInfoCertificate;

        return $this;
    }

    /**
     * Get clinicInfoCertificate
     *
     * @return \AppBundle\Entity\siteInformation
     */
    public function getClinicInfoCertificate()
    {
        return $this->clinicInfoCertificate;
    }

    /**
     * Set clinicInfoGrant
     *
     * @param \AppBundle\Entity\siteInformation $clinicInfoGrant
     *
     * @return File
     */
    public function setClinicInfoGrant(\AppBundle\Entity\siteInformation $clinicInfoGrant = null)
    {
        $this->clinicInfoGrant = $clinicInfoGrant;

        return $this;
    }

    /**
     * Get clinicInfoGrant
     *
     * @return \AppBundle\Entity\siteInformation
     */
    public function getClinicInfoGrant()
    {
        return $this->clinicInfoGrant;
    }
}
