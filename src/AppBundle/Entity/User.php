<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 01.08.2016
 * Time: 15:42
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

//    private $login;
//    private $password;


    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $mobilePhone;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $fio;
    

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $shortAbout;

    /**
     * @ORM\Column(type="string", length=255,   nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $dateOfBirth;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Speciality", inversedBy="users")
     */
    private $speciality;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Service", mappedBy="responsible")
 */
    private $services;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="doctor")
     */
    private $orders;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\medUnit", inversedBy="users")
     */
    private $medUnit;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Timetable", mappedBy="user")
     */
    private $timetables;


    /**
     * @ORM\Column(type="boolean", options={"default" = false}, nullable=true)
     */
    private $isBoss;


//    private $grants;
//
//    private $messages



    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return User
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set name
     *
     * @param string $fio
     *
     * @return User
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    

    /**
     * Set shortAbout
     *
     * @param string $shortAbout
     *
     * @return User
     */
    public function setShortAbout($shortAbout)
    {
        $this->shortAbout = $shortAbout;

        return $this;
    }

    /**
     * Get shortAbout
     *
     * @return string
     */
    public function getShortAbout()
    {
        return $this->shortAbout;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return User
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set speciality
     *
     * @param string $speciality
     *
     * @return User
     */
    public function setSpeciality($speciality)
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return string
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    public function getAbsolutePath(){
        return null === $this->photo
            ? null
            : $this->getUploadRootDir().'/'.$this->photo;
    }

    public function getWebPath(){
        return null === $this->photo
            ? null
            : $this->getUploadDir().'/'.$this->photo;
    }

    protected function getUploadRootDir(){
        // the absolute directory path where uploadeddocuments should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir(){
        // get rid of the __DIR__ so it doesn't screw up when displaying uploaded doc/image in the view.
        return 'uploads';
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeFile()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return User
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\Order $order
     *
     * @return User
     */
    public function addOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\Order $order
     */
    public function removeOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    

    /**
     * Set medUnit
     *
     * @param \AppBundle\Entity\medUnit $medUnit
     *
     * @return User
     */
    public function setMedUnit(\AppBundle\Entity\medUnit $medUnit = null)
    {
        $this->medUnit = $medUnit;

        return $this;
    }

    /**
     * Get medUnit
     *
     * @return \AppBundle\Entity\medUnit
     */
    public function getMedUnit()
    {
        return $this->medUnit;
    }

    /**
     * Add timetable
     *
     * @param \AppBundle\Entity\Timetable $timetable
     *
     * @return User
     */
    public function addTimetable(\AppBundle\Entity\Timetable $timetable)
    {
        $this->timetables[] = $timetable;

        return $this;
    }

    /**
     * Remove timetable
     *
     * @param \AppBundle\Entity\Timetable $timetable
     */
    public function removeTimetable(\AppBundle\Entity\Timetable $timetable)
    {
        $this->timetables->removeElement($timetable);
    }

    /**
     * Get timetables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimetables()
    {
        return $this->timetables;
    }

    /**
     * Set isBoss
     *
     * @param boolean $isBoss
     *
     * @return User
     */
    public function setIsBoss($isBoss)
    {
        $this->isBoss = $isBoss;

        return $this;
    }

    /**
     * Get isBoss
     *
     * @return boolean
     */
    public function getIsBoss()
    {
        return $this->isBoss;
    }
}
