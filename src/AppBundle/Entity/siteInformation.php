<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 26.09.2016
 * Time: 20:03
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="siteInformation")
 */
class siteInformation

{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $info;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\File", mappedBy="clinicInfoPhoto")
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\File", mappedBy="clinicInfoCertificate", cascade={"persist", "remove"})
     */
    private $certificates;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\File", mappedBy="clinicInfoGrant", cascade={"persist", "remove"})
     */
    private $grants;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->certificates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->grants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return siteInformation
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set photo
     *
     * @param \AppBundle\Entity\File $photo
     *
     * @return siteInformation
     */
    public function setPhoto(\AppBundle\Entity\File $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \AppBundle\Entity\File
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Add certificate
     *
     * @param \AppBundle\Entity\File $certificate
     *
     * @return siteInformation
     */
    public function addCertificate(\AppBundle\Entity\File $certificate)
    {
        $this->certificates[] = $certificate;

        return $this;
    }

    /**
     * Remove certificate
     *
     * @param \AppBundle\Entity\File $certificate
     */
    public function removeCertificate(\AppBundle\Entity\File $certificate)
    {
        $this->certificates->removeElement($certificate);
    }

    /**
     * Get certificates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCertificates()
    {
        return $this->certificates;
    }

    /**
     * Add grant
     *
     * @param \AppBundle\Entity\File $grant
     *
     * @return siteInformation
     */
    public function addGrant(\AppBundle\Entity\File $grant)
    {
        $this->grants[] = $grant;

        return $this;
    }

    /**
     * Remove grant
     *
     * @param \AppBundle\Entity\File $grant
     */
    public function removeGrant(\AppBundle\Entity\File $grant)
    {
        $this->grants->removeElement($grant);
    }

    /**
     * Get grants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrants()
    {
        return $this->grants;
    }
}
