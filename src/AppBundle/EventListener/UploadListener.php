<?php

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 16.06.2016
 * Time: 14:31
 */

namespace AppBundle\EventListener;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Entity\File as uploadedFile;


class UploadListener
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function onUpload(PostUploadEvent $event)
    {
        $request = $event->getRequest();

        $file = $event->getFile();
        $filename = $file->getFilename();

        $dropzoneDestination = $request->get('dropzoneDestination');

        if ($dropzoneDestination === "clinicPhoto"){
            $siteInformationId = $request->get('siteInformationId');
            $siteInfo = $this->om->getRepository('AppBundle:siteInformation')->find($siteInformationId);
            $oldPhoto = $siteInfo->getPhoto();

            $uploadedfile = new uploadedFile();
            if ($oldPhoto) {
                $this->om->remove($oldPhoto);
                $this->om->flush();
            }
            $uploadedfile->setPath($filename);
            $siteInfo->setPhoto($uploadedfile);
            $uploadedfile->setClinicInfoPhoto($siteInfo);
            $this->om->persist($uploadedfile);
            $this->om->persist($siteInfo);
            $this->om->flush();
        } elseif ($dropzoneDestination === "clinicSertificates"){
            $siteInformationId = $request->get('siteInformationId');
            $siteInfo = $this->om->getRepository('AppBundle:siteInformation')->find($siteInformationId);
            $uploadedfile = new uploadedFile();
            $uploadedfile->setPath($filename);

            $siteInfo->addCertificate($uploadedfile);
            $uploadedfile->setClinicInfoCertificate($siteInfo);
            $this->om->persist($uploadedfile);
            $this->om->persist($siteInfo);
            $this->om->flush();

        } elseif ($dropzoneDestination === "clinicGrants"){
            $siteInformationId = $request->get('siteInformationId');
            $siteInfo = $this->om->getRepository('AppBundle:siteInformation')->find($siteInformationId);
            $uploadedfile = new uploadedFile();
            $uploadedfile->setPath($filename);

            $siteInfo->addGrant($uploadedfile);
            $uploadedfile->setClinicInfoGrant($siteInfo);
            $this->om->persist($uploadedfile);
            $this->om->persist($siteInfo);
            $this->om->flush();

        }
        else {
            $userId = $request->get('userId');
            $user = $this->om->getRepository('AppBundle:User')->find($userId);
            $file = $user->getAbsolutePath();
            if ($file) {
                unlink($file);
            }
            $user->setPhoto($filename);
            $this->om->persist($user);
            $this->om->flush();
        }




    }
}