<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 09.08.2016
 * Time: 14:34
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Listener responsible to change the redirection at the end of the registration
 */
class UserRegistrationListener implements EventSubscriberInterface
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationConfirm'];
    }

    public function onRegistrationConfirm(FormEvent $event)
    {
        $url = $this->router->generate('user_index');

        $event->setResponse(new RedirectResponse($url));
    }
}