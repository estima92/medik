<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Заголовок'
            ))
            ->add('link', TextType::class, array(
                'label' => 'Ссылка латиницей'
            ))
            ->add('text', TextAreaType::class, array(
                'label' => 'Текст Статьи',
                'attr' => array(
                    'class' => 'tinymce'
                )
            ))
            ->add('status')
            ->add('date', 'date')
            ->add('category', EntityType::class, array(
        'label' => 'Категория',
        'class' => 'AppBundle:Category',
        'choice_label' => 'name'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content'
        ));
    }
}
