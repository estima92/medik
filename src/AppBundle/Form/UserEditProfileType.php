<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 02.08.2016
 * Time: 10:55
 */

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserEditProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone')
            ->add('mobilePhone')
            ->add('fio', TextType::class)
            ->add('shortAbout',TextAreaType::class , array(
                'label' => 'Кратко о себе',
                'attr' => array('maxlength' => '150')))
            ->add('dateOfBirth', DateType::class, array(
                'label' => 'Дата рождения',
                'widget' => 'choice',
                'years' => range(1930,2000),
                'format' => 'dd-MM-yyyy',
            ))
            ->add('medUnit', EntityType::class, array(
                'label' => 'Отделение',
                'class' => 'AppBundle:MedUnit',
                'choice_label' => 'name'))
            ->add('speciality', EntityType::class, array(
                'label' => 'Специальность',
                'class' => 'AppBundle:Speciality',
                'choice_label' => 'name'))
            ->add('about',TextAreaType::class , array(
                'label' => 'О себе',
                'attr' => array('maxlength' => '150', 'novalidate')))
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

//    public function getBlockPrefix()
//    {
//        return 'fos_user_profile';
//    }

}