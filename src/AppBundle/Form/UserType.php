<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone')
            ->add('mobilePhone')
            ->add('fio', TextType::class)
            ->add('shortAbout',TextAreaType::class , array(
                'label' => 'Кратко о себе',
                'attr' => array('maxlength' => '150')))
            ->add('dateOfBirth', DateType::class, array(
                'label' => 'Дата рождения',
                'widget' => 'choice',
                'years' => range(1930,2000),
                'format' => 'dd-MM-yyyy',
            ))
            ->add('medUnit', EntityType::class, array(
                'label' => 'Отделение',
                'class' => 'AppBundle:MedUnit',
                'choice_label' => 'name'))
            ->add('speciality', EntityType::class, array(
                'label' => 'Специальность',
                'class' => 'AppBundle:Speciality',
                'choice_label' => 'name'))
            ->add('about',TextAreaType::class , array(
                'label' => 'О себе',
                'attr' => array('class' => 'tinymce', 'novalidate' => 'novalidate', 'required' => 'true')))
            ->add('roles', ChoiceType::class, array(
                'choices'  => array(
                    'ROLE_USER' =>  'Врач',
                    'ROLE_REGISTER' =>  'Регистратура',
                    'ROLE_ADMIN' =>  'Админ',),
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('isBoss', CheckboxType::class, array(
                'label' => 'Руководитель?'
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }
}
