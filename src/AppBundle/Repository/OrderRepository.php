<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 10.09.2016
 * Time: 10:24
 */

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    public function findNearestWeekInMedUnit1($parameters)
    {
        $query = $this->createQueryBuilder('o')
//            ->innerJoin('o.medUnit', 'm')
//            ->where('m.id = :medUnitId')
//            ->andWhere('DATE_DIFF(o.date , CURRENT_DATE()) <= 7')
//            ->andWhere('DATE_DIFF(o.date , CURRENT_DATE()) >= 0')
//            ->setParameters($parameters)
//            ->addOrderBy('o.date', 'ASC')
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function findNearestWeek($parameters)
    {

        $query = $this->createQueryBuilder('o')
            ->innerjoin('o.doctor', 'u')
            ->where('u.id = :user_id')
            ->andWhere('DATE_DIFF(o.startDate , CURRENT_DATE()) <= 7')
            ->andWhere('DATE_DIFF(o.startDate , CURRENT_DATE()) >= 0')
            ->andWhere('o.isApproved = true')
            ->setParameters($parameters)
            ->addOrderBy('o.startDate', 'ASC')
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function findAllInCurrentMonth($parameters)
    {

        $query = $this->createQueryBuilder('o')
            ->innerjoin('o.doctor', 'u')
            ->where('u.id = :user_id')
            ->andWhere('MONTH(o.startDate) = :currentMonth')
            ->setParameters($parameters)
            ->addOrderBy('o.startDate', 'ASC')
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }}