<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TimetableRepository extends EntityRepository
{
    public function findAllInCurrentDate($parameters)
    {

        $query = $this->createQueryBuilder('t')
            ->innerjoin('t.user', 'u')
            ->where('u.id = :user_id')
            ->andWhere('DAY(t.startDate) = :currentDay')
            ->setParameters($parameters)
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function findAllInCurrentMonth($parameters)
    {

        $query = $this->createQueryBuilder('t')
            ->innerjoin('t.user', 'u')
            ->where('u.id = :user_id')
            ->andWhere('MONTH(t.startDate) = :currentMonth')
            ->setParameters($parameters)
            ->addOrderBy('t.startDate', 'ASC')
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function findNearestWeek($parameters)
    {

        $query = $this->createQueryBuilder('t')
            ->innerjoin('t.user', 'u')
            ->where('u.id = :user_id')
            ->andWhere('DATE_DIFF(t.startDate , CURRENT_DATE()) <= 7')
            ->andWhere('DATE_DIFF(t.startDate , CURRENT_DATE()) >= 0')
            ->setParameters($parameters)
            ->addOrderBy('t.startDate', 'ASC')
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function findNearestWeekInMedUnit($parameters)
    {

        $query = $this->createQueryBuilder('t')
            ->innerjoin('t.user', 'u')
            ->where('u.medUnit = :medUnitId')
            ->andWhere('DATE_DIFF(t.startDate , CURRENT_DATE()) <= 7')
            ->andWhere('DATE_DIFF(t.startDate , CURRENT_DATE()) >= 0')
            ->setParameters($parameters)
            ->addOrderBy('t.user', 'ASC')
            ->getQuery();
        $result = $query->getResult();
        return $result;
    }

}