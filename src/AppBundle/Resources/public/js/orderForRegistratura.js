/**
 * Created by Asus on 16.09.2016.
 */

var isOrderVisible = false;
var orderParams = {
    medUnit : 0
};
var medUnitName = "";
var Doctor = "";
$('#medUnitSelect').change(function(){
    var val = $(this).val();
    orderParams.medUnit = val;
    medUnitName = this.options[this.selectedIndex].text;
    $.ajax({
        type: "POST",
        url: "/getDoctorsWithMedUnit" + val,
        success: function(data) {
            $('#medUnitAndDoctorsContainer').children().last().remove();
            $('#medUnitAndDoctorsContainer').append(data.doctorHTML);
            $('#dateAndHourContainer').remove();
            $('#fioAndPhoneContainer').before(data.daysHTML);
        }
    });
});


function proceedOrder() {
    if( orderParams.medUnit == 0) {
//            $('#medUnitSelect').parent().addClass('has-error');
        alert('Пожалуйста, выберите отделение для посещения!');
        return null;
    }
    if (typeof orderParams.day === 'undefined' || orderParams.day === null) {
        alert('Пожалуйста выберите день посещения!');
        return null;
    }
    if (typeof orderParams.hour === 'undefined' || orderParams.hour === null) {
        alert('Пожалуйста выберите час посещения!');
        return null;
    }
    if( $('#orderFio').val().length < 10 ) {
        $('#orderFio').parent().addClass('has-error');
        alert('ФИО слишком короткие!');
        return null;
    }
    if( $('#orderPhone').val().length < 6 ) {
        $('#orderPhone').parent().addClass('has-error');
        alert('Телефон слишком короткий!');
        return null;
    }
    orderParams.fio = $('#orderFio').val();
    orderParams.phone = $('#orderPhone').val();
    $('#modalOrderInfoFio').text(orderParams.fio);
    $('#modalOrderInfoPhone').text(orderParams.phone);
    $('#modalOrderInfoMedUnit').text(medUnitName);
    $('#modalOrderInfoDoctor').text(Doctor);
    $('#modalOrderInfoDate').text(orderParams.day + " " + orderParams.hour+ ":00 ");
    $('#orderConfirm').modal();

}
function proceedOrderToAjax(){
    $.ajax({
        type: "POST",
        url: "/proceedOrder",
        dataType: "json",
        data: orderParams,
        success: function() {
            $('#orderConfirm').modal('hide');
            $('.orderWindow').hide().remove();
            $.ajax({
                type: "GET",
                url: "/getOrderWindow",
                success: function(data) {
                    $('.content').append(data);
                }
            });
        }
    });
}



$(document).on('click',".dayButton", function(){
    var id = event.target.id;
    var a = event.target.id.match(/\d+/)[0];
    orderParams.day = event.target.value;
    $(".dayButton").removeClass('orderButtonActive');
    $("#"+id).addClass('orderButtonActive');
    $("[id^='hourButtons']").hide();
    $("#hourButtons"+a).show();
});

$(document).on('click',".hourButton", function(){
    var id = event.target.id;
    var a = event.target.id.match(/\d+/)[0];
    orderParams.hour = event.target.value;
    $(".hourButton").removeClass('orderButtonActive');
    $("#"+id).addClass('orderButtonActive');
});


